[build-system]
requires = ["flit_core >=3.2,<4"]
build-backend = "flit_core.buildapi"

[project]
name = "travo"
description = 'Management tools for gitlab-based assignment workflows'
dynamic = [
    "version"
]
authors = [
  { name = "Pierre Thomas Froidevaux"},
  { name = "Alexandre Blondin-Massé"},
  { name = "Jean Privat"},
  { name = "Nicolas M. Thiéry"},
  { name = "Chiara Marmo"},
  { name = "Jérémy Neveu"},
]

maintainers = [
  { name = "Nicolas M. Thiéry", email = "Nicolas.Thiery@universite-paris-saclay.fr"},
  { name = "Chiara Marmo", email = "chiara.marmo@universite-paris-saclay.fr"},
]
readme = "README.md"
license = {text = "CC"}
classifiers = [
  "Development Status :: 3 - Alpha",
  "Intended Audience :: Information Technology",
  "Topic :: Scientific/Engineering",
  "Programming Language :: Python :: 3",
]  # classifiers list: https://pypi.python.org/pypi?%3Aaction=list_classifiers

requires-python = ">=3.8"

dependencies = [
  "colorlog",
  "dataclasses",
  "deprecation",
  "typing_utils",
  "requests",
  "tqdm",
  "anybadge",
  "i18nice[YAML]",
]

[project.optional-dependencies]
# feature dependency groups
jupyter = [
    "ipywidgets",
    "nbgrader",
    "numpy",
    "pandas",
    "ipylab"
]

test = [
    "pytest",
    "mypy",
    "nbgrader",
    "types-requests",
    "types-pkg_resources",
    "types-toml",
    "ipydatagrid",
    "python-gitlab",
    "tox-docker",
]

doc = [
    "sphinx",
    "numpydoc",
    "sphinx-autoapi",
    "myst-parser>=0.13.1",
    "pydata-sphinx-theme",
    "sphinx-copybutton>0.2.9",
    "nbconvert",
    "myst_nb",
    "jupytext>=1.11.2",
    "ipykernel",
]

[project.urls]
Repository = "https://gitlab.com/travo-cr/travo.git/"

[project.scripts]
travo = "travo.console_scripts:travo"
travo-echo-travo-token = "travo.console_scripts:travo_echo_travo_token"

[tool.pytest.ini_options]
addopts = "--doctest-modules --doctest-continue-on-failure"
doctest_optionflags = "NORMALIZE_WHITESPACE IGNORE_EXCEPTION_DETAIL ELLIPSIS"

# Workaround
#
# By default, mypy assigns treats each script as a module named
# __main__. This prevents checking several scripts at once. The usual
# workaround is to use the option `scripts_are_modules`. Then the
# script is treated as a module bearing its name. But then there is a
# conflict between the script travo and the module travo.
#
# To work around this, we have a symbolic link travo-py -> travo, and
# we only check scripts containing a '-' in their names ...
[tool.mypy]
files = "*.py, travo/*.py, scripts/*-*"
warn_return_any = "True"
warn_unused_configs = "True"
disallow_untyped_defs = "True"
disallow_untyped_calls = "True"
disallow_incomplete_defs = "True"
check_untyped_defs = "True"
no_implicit_optional = "True"
scripts_are_modules = "True"

[tool.black]
line-length = 88
preview = true
required-version = "22.12.0"

[tool.tox]
legacy_tox_ini = """
    [tox]
    envlist =
        py311
        py312

    [docker:gitlab]
    image = gitlab/gitlab-ce:latest
    environment =
        GITLAB_ROOT_PASSWORD=dr0w554p!&ew=]gdS
    healthcheck_cmd = curl --retry 10 --retry-all-errors http://localhost/api/v4/projects

    [testenv]
    # install pytest in the virtualenv where commands will be executed
    deps =
        pytest
        pytest-cov
        mypy
        nbgrader
        types-requests
        types-pkg_resources
        types-toml
        ipydatagrid
        python-gitlab

    docker =
        gitlab

    commands_pre =
        python build_tools/create_basic_gitlab.py
    commands =
        mypy
        pytest {posargs:travo} --cov --cov-report term --cov-report xml:coverage.xml --junitxml=report.xml
"""
